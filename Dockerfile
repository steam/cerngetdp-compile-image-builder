# We basically only add python3 to their Docker image since you cannot change the default user of a Docker image 
# see also https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2750, it might be hopefully be added in a future realease. 

FROM onelab/debian.stretch.64bit

# need root priviliges to install python
USER root 

# trying to fix sources list, results in packages not being found
# RUN rm /etc/apt/sources.list 
# RUN echo "deb http://archive.debian.org/debian-security stretch/updates main" > /etc/apt/sources.list.d/stretch.list 

RUN apt-get update
RUN apt-get -y upgrade
# prerequisites of Python 3.11.8
RUN apt-get install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev liblzma-dev 
RUN apt-get clean

# install python and compile manually since Python3 only goes to 3.5 ... 
ENV PYTHON_VERSION=3.11.8

RUN curl -O https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz 
RUN tar -xf Python-${PYTHON_VERSION}.tar.xz
RUN cd Python-${PYTHON_VERSION} && ./configure --enable-optimizations && make -j 4 && make altinstall

# grab gsl for getdp 
RUN apt-get install -y libgsl-dev

# switch back to default user
USER geuzaine
