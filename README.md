# CERNGetDP Compile Image Builder

This repository builds Docker images for the compilation of CERNGetDP, should we need different versions from the ONELAB provided ones found at https://hub.docker.com/u/onelab. Currently, these are 

- default image: based on onelab/debian.stretch.64bit: add python3 and python3-pip
